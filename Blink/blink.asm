	;;
	;; Description
	;; 	blink onboard led (pin 13 on Arduino Leonardo)

	;; 
	;; Notes
	;; 	I'll do my best to describe what's going on, but i'm prone to
	;; 	errors. So, always, do a check first.

	.DEVICE	ATmega32U4	; needed by the assembler (gavrasm)

	;; drop the frequency from 16Mhz to 65Khz
	;; the LED is in PortC
	;; make the pin output
	;; set to high
	;; count to zero
	;; set to low
	;; count to zero
	;; repeat from set to high


	;; PC7 (PortC 7th bit) also acts as ICP3/CLKO/OC4A
	;; so, one might have to configure it with fuses (extra care should be
	;; taken)

	;; Chapter 10.2.1

	;; set pin 7 as output
	sbi	DDRC,7

	;; reduce CLK_cpu by 256
	;; activate
	;; I think pin 7 must be on, in order to edit 3,2,1,0.
 	ldi	r16,$80
	;; set 256
 	ldi	r17,$08
	;; actual activate (editable for 4 cycles)
 	sts	CLKPR,r16
	;; actual set (to 1/256 CLK_cpu)
	sts	CLKPR,r17
	
	;; mask used for toggling PORTC7 (with XOR)
	ldi	r17,$80

loop:	
	;; toggle PORTC7 with (PC7 XOR 1)
	in	r16,PORTC	; 1 cycle
	eor	r16,r17		; 1 cycle
	out	PORTC,r16	; 1 cycle

	;; count 256 steps
	ldi	r16,$FF		; 1 cycle

	;; clear z (zero) flag possibly set by eor
	clz			; 1 cycle
loop_inner:
	dec r16			; sets flags : Z, N, V, S
				; 1x255 cycles
	brne loop_inner		; 1 (false) / 2 (true)
				; 2x254 + 1 cycles
	rjmp loop		; can jump from -2K to +2K words
				; 2 cycles

	;; total : 5 + 255 + 2x254 + 1 + 2 per state
	;; = 260 + 508 + 3
	;; = 768 + 3
	;; = 771 cycles
	;; 
	;; 2x771 = 1542 cycles
	;;
	;; i suppose CLK_cpu = 16e6/256 = 62500 Hz
	;; i suppose LED frequency is CLK_CPU/cycles = ~ 40.532 Hz

	;; after checking with a frequency meter, it is indeed ~40.53 Hz !!!
	;; it is visible. Like a trembling old screen:)
