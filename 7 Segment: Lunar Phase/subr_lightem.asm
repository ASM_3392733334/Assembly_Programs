	;; SUBROUTINE
	;; 
	;; lightem uses r16 as input of 1 byte
lightem:
	;; light 'em in groups

	;; parameter's bits : 2,3,4,5 refer to  PortB{4,5,6,7}
	;; param's bit      : 7       refers to PortC7
	;; param's bits     : 0,6     refer to  PortD{7,6}
	;; param's bit 	    : 1       refers to PortE6

	;; PORT B
	mov tmp,param
	andi tmp,0x3C		; mask bits 2..5
	lsl tmp			; shift left 2 places
	lsl tmp

	in tmp2,PORTB
	andi tmp2,0x0F		; mask bits 0..4
	or tmp2,tmp
	out PORTB,tmp2
	
	;; PORT C
	mov tmp,param		; don't touch 'param'
	andi tmp, 0x80		; the 7th bit for PORT C
	in tmp2,PORTC		; read PORT C
	andi tmp2,0x3F		; the remaining 7 bits from PORT C
	or tmp2,tmp		; add them together
	out PORTC,tmp2		; write to PORT C
	;; PORT D
	;; 
	;; param0 -> D7
	mov tmp,param
	andi tmp,1		; param 0
	clc			; clear carry from previous operations
	ror tmp			; 0 -> C ??
	ror tmp			; C -> 7 ??

	mov tmp2,param		; param 6
	andi tmp2,0x40		; max bit 6
	or tmp,tmp2		; mix bits 0 and 6 in tmp
	
	in tmp2,PORTD		; read PORT D
	andi tmp2,0x3F		; mask bits 6..0
	or tmp2,tmp		; mix tmp + tmp2
	out PORTD,tmp2		; write PORT D
	;; param6 -> D6
	;; PORT E
	mov tmp,param
	andi tmp,2		; get only bit 1 from 'param'
	clc			; clear carry, just in case..
	ror tmp			; bit 1 -> 0 (Rorate Right through Carry)
	ror tmp			; bit 0 -> 7
	ror tmp			; bit 7 -> 6
	ror tmp			; ????

	in tmp2,PORTE		; read PORT E
	andi tmp2,0xBF		; get the rest 7,5..0 bits from PORT E
	or tmp2, tmp		; mix bit 6 with the rest from PORT E
	out PORTE,tmp2
	;; 
	ret
	;;
	;; SUBROUTINE_END
