	;;
	;; Description
	;; 	description

	;;
	;; Notes
	;; 	I'll do my best to describe what's going on, but i'm prone to
	;; 	errors. So, always, do a check first.

	.DEVICE	ATmega32U4	; needed by the assembler (gavrasm)

	;; short flow chart
	;; 1.
	;; 2.
	;;


	;; warnings
	;; *
	;;

	;; reference (datasheet..)
	;;

	;; with the numbers from the flow chart
	;; describe each section

	;; code here
	;;

	;; DEFINE aliases
	;;
	.DEF tmp=r16		; define r16 as temporary space
	.DEF tmp2=r17
	.DEF param=r18		; used for SUBROUTINES
	.DEF XL=r26
	.DEF XH=r27
	.DEF YL=r28
	.DEF YH=r29
	.DEF ZL=r30
	.DEF ZH=r31
	;;
	;; DEFINE_END

	;; REDUCE clock speed
	;;
	.include code_reduce_clk.asm
	;;
	;; REDUCE_END
	
	;; INITIALIZE PORTs as output
	;;
	.include code_init_ports.asm
	;;
	;; INITIALIZE_END


	;; Lunar phase to LED mappings
	;; 
map:	.DB 0x80,0x20,0x60,0x70
	.DB 0x74,0x75,0x7D,0x7F
	.DB 0x5F,0x1F,0x0F,0x0B
	.DB 0x0A,0x02

	;; MAIN
	;; 
	ldi YH, high(map<<1)	; start address
	ldi YL, low(map<<1) 	;
	mov XH,YH		; end address
	mov XL,YL		;
	adiw XH:XL,13		;
start:	
	mov ZH,YH		; needed for lpm command (Load Program Memory)
	mov ZL,YL		;
iterate:
	;;	lpm param,Z
	lpm param,Z
	call lightem		; light using param=mem[i]
	call delay		;
	cp ZH,XH
	brlo next
	cp ZL,XL
	brlo next
	;; end address reached
	rjmp start
next:
	adiw ZH:ZL,1
	rjmp iterate		;
	;;
	;; MAIN_END

	;; SUBROUTINE DELAY
	;;
delay:
	clr tmp2		; 1 clock
	;; outer loop begin
dloop2:	
	clr tmp			; 1 * dloop2(255)
	;; inner loop begin
dloop:	
	inc tmp			; 255 * dloop2(255)
	cpi tmp,255		; 255 * dloop2(255)
	brne dloop		; (254*2 + 1) * dloop2(255)
	;; inner loop end
	inc tmp2		; 255
	cpi tmp2,255		; 255
	brne dloop2		; 254 * 2 + 1
	;; outer loop end	
	ret			; 4
	;;
	;; SUBROUTINE_END	; total of 261124 cycles ~ 4s at 65KHz

	
	;; SUBROUTINE LIGHTEM
	;;
	.include subr_lightem.asm
	;;
	;; SUBROUTINE_END

	;;
	;; code ends

	;; final notes
	;; more info

