Given the Arduino pin order, i assign the following bits on each LED, so that
i pass the information on a single byte :

>     o(7)           o(PC7)
>         -(5)-           -(PB7)-
>       |       |       |         |
>      (6)     (4)    (PD6)     (PB6)
>       |       |       |         |
>         -(2)-           -(PB4)-
>       |       |       |         |
>      (0)     (3)    (PD7)     (PB5)
>       |       |       |         |
>         -(1)-           -(PE6)-

---
## the [w:lunar phases](https://en.wikipedia.org/wiki/Lunar_phase)

>        0          1a        1b       1c        2         3a        3b        4        3'b       3'a        2'       1'c       1'b       1'a   

>        Nm        Wxc       Wxc      Wxc        1q       Wxg       Wxg        Fm       Wng       Wng        3q       Wnc       Wnc       Wnc 

>     o            ---       ---      ---       ---       ---       ---       ---                                         
>                          |        |     |   |     |   |     |   |     |   |     |   |     |         |          
>                          |        |     |   |     |   |     |   |     |   |     |   |     |         |          
>                                               ---       ---       ---       ---       ---       ---       ---
>                                                       |         |     |   |     |   |     |   |     |   |     |   |     |         |  
>                                                       |         |     |   |     |   |     |   |     |   |     |   |     |         |  
>                                                                             ---       ---       ---       ---       ---       ---       ---

     Nm : New moon  
    Wxc : Waxing crescent
     1q : First quarter  
    Wxg : Waxing gibbous  
     Fm : Full moon  
    Wng : Waning gibbous  
     3q : Third quarter  
    Wnc : Waning crescent  

---
## bit mappings (you can change these to your liking:)
     Nm   <-- 0b 1000 0000   = 0x80  
    Wxc 1 <-- 0b 0010 0000   = 0x20  
    Wxc 2 <-- 0b 0110 0000   = 0x60  
    Wxc 3 <-- 0b 0111 0000   = 0x70  
     1q   <-- 0b 0111 0100   = 0x74  
    Wxg 1 <-- 0b 0111 0101   = 0x75  
    Wxg 2 <-- 0b 0111 1101   = 0x7D  
     Fm   <-- 0b 0111 1111   = 0x7F  
    Wng 1 <-- 0b 0101 1111   = 0x5F  
    Wng 2 <-- 0b 0001 1111   = 0x1F  
     3q   <-- 0b 0000 1111   = 0x0F  
    Wnc 1 <-- 0b 0000 1011   = 0x0B  
    Wnc 2 <-- 0b 0000 1010   = 0x0A  
    Wnc 3 <-- 0b 0000 0010   = 0x02  
