### Programs used

#### [gavrasm](http://www.avr-asm-tutorial.net/gavrasm/index_en.html) : an avr assembler (many thnx for this!!)
##### Licence :
>
- Copyright for all versions: (C)2002..2018 by Gerhard Schmidt
>
- Free use of the source code and the compiled versions for non-
  commercial purposes. Distribution allowed if the copyright
  information is included.
>
- No warranty for correct functioning of the software.
>
- Report errors (especially any compiler errors) and your
  most-wanted features to gavrasm(at)avr-asm-tutorial.net (Subject
  "gavrasm 3.8"). If errors occur, please include the source code,
  and any included files, the listing and the error file.
          
#### [avrdude](http://www.nongnu.org/avrdude/) : an avr uploader

#### What the program does :

#### On which platform :
Arduino Leonard

#### Kernel module
cdc-acm

#### Command to run :
make help
